var net = require("net");
var ProtoBuf = require("protobufjs");
var LB_HOME = process.env.LOGICBLOX_HOME;
var _ = require('lodash');

var protoBuilder = ProtoBuf.protoFromFile({
    root: LB_HOME + "/lib/protobuf",
    file: "blox/connect/ConnectBlox.proto"
});

var RegularRequest = protoBuilder.build("blox.connect.Request");
var RegularResponse = protoBuilder.build("blox.connect.Response");
var AdminRequest = protoBuilder.build("blox.connect.AdminRequest");
var AdminResponse = protoBuilder.build("blox.connect.AdminResponse");

// Socket protocol utilities
function intToBuf(n) {
    var b = new Buffer(4);
    b[0] = (n >> 24) & 0xff;
    b[1] = (n >> 16) & 0xff;
    b[2] = (n >> 8) & 0xff;
    b[3] = (n >> 0) & 0xff;
    return b;
}

function bufToInt(b) {
    return ((b[0] & 0xff) << 24) | ((b[1] & 0xff) << 16) | ((b[2] & 0xff) << 8) | ((b[3] & 0xff) << 0);
}

function columnToValues(col) {
    if (col.string_column) {
        return {
            type: 'string',
            values: col.string_column.values
        };
    } else if (col.int64_column) {
        return {
            type: 'int',
            values: col.int64_column.values
        };
    } else if (col.float64_column) {
        return {
            type: 'float',
            values: col.float64_column.values
        };
    } else if (col.decimal_column) {
        return {
            type: 'decimal',
            values: col.decimal_column.values
        };
    } else if (col.bool_column) {
        return {
            type: 'bool',
            values: col.bool_column.values
        };
    } else if (col.datetime_column) {
        return {
            type: 'datetime',
            values: col.datetime_column.values
        };
    } else if (col.entity_column) {
        return {
            type: 'entity',
            values: columnToValues(col.entity_column.refmode_values)
        };
    } else {
        return col;
    }
}

function jsonValue(type, val) {
    switch (type) {
        case 'int':
            return val.toString();
        case 'decimal':
            return val.toString();
        case 'datetime':
            return val.low;
        default:
            return val;
    }
}

function transaction(workspace, readonly, commands) {
    return {
        transaction: {
            workspace: workspace,
            readonly: readonly,
            exclusive: false,
            command: commands,
            command_after_fixpoint: []
        }
    };
}

function flipColumns(cols) {
    var colValues = cols.map(columnToValues);
    var results = [];
    for (var i = 0; i < colValues[0].values.length; i++) {
        var row = [];
        for (var j = 0; j < colValues.length; j++) {
            var val = {
                type: colValues[j].type,
                value: jsonValue(colValues[j].type, colValues[j].values[i])
            };
            // val.jsValue = jsValue(val);
            row.push(val);
        }
        results.push(row);
    }
    return results;
}

function fillPlaceHolders(logic, args) {
    var idx = 0;
    return logic.replace(/\?/g, function() {
        var val = args[idx];
        idx++;
        if (_.isString(val)) {
            return '"' + val.replace(/"/g, '\\"') + '"';
        } else if (_.isNumber(val)) {
            return "" + val;
        } else if (_.isBoolean(val)) {
            return "" + val;
        } else {
            throw Error("Placeholder value not supported: " + val);
        }
    });
}

var DEFAULT_ADMIN_PORT = 55181;
var DEFAULT_PORT = 55179;

module.exports = function(options, callback) {
    if (_.isFunction(options)) {
        callback = options;
        options = {};
    }
    if (!_.isFunction(callback)) {
        return _.partial(module.exports, options);
    }
    options = options || {};
    var reqId = 0;
    var outstandingRequests = {}; // reqid -> callback
    var api;

    var port = options.port || process.env.LB_CONNECTBLOX_SERVER_PORT || DEFAULT_PORT;
    var isAdmin = !! options.admin;
    if (isAdmin) {
        port = options.port || process.env.LB_CONNECTBLOX_SERVER_ADMIN_PORT || DEFAULT_ADMIN_PORT;
    }

    var client = net.connect({
        port: port
    }, function() {
        callback(null, api);
    });

    var defaultApi = {
        request: function(json, callback) {
            var r = new RegularRequest(json);
            var buffer = r.encode().toBuffer();
            reqId++;
            outstandingRequests[reqId] = callback;
            client.write(intToBuf(reqId));
            client.write(intToBuf(buffer.length));
            client.write(buffer);
        },
        // Workspace management
        createWorkspace: function(workspace, callback) {
            defaultApi.request({
                create: {
                    name: workspace,
                    measure_engine: false,
                    snapshot_isolation: false,
                    overwrite: false
                }
            }, function(err, result) {
                if(err) {
                    return callback(err);
                }
                callback(null, result.create.name);
            });
        },
        resetWorkspace: function(workspace, callback) {
            defaultApi.request({
                create: {
                    name: workspace,
                    measure_engine: false,
                    snapshot_isolation: false,
                    overwrite: true
                }
            }, callback);
        },
        removeWorkspace: function(workspace, callback) {
            defaultApi.request({
                "delete": {
                    name: workspace,
                }
            }, callback);
        },
        exportWorkspace: function(workspace, dir, callback) {
            defaultApi.request({
                "exportws": {
                    "name": workspace,
                    "dest_filepath": dir
                }
            }, callback);
        },
        importWorkspace: function(workspace, dir, callback) {
            defaultApi.request({
                "importws": {
                    "src_filepath": dir,
                    "name": workspace
                }
            }, callback);
        },
        /**
         * Options:
         * - name: block name
         * - logic: logic
         * - inactive: set to true to make inactive block 
         */
        addBlock: function(workspace, params, callback) {
            defaultApi.request(transaction(workspace, false, [{
                add_block: {
                    block_name: params.name || null,
                    active: !params.inactive,
                    logic: params.logic || null
                }
            }]), function(err, result) {
                if(err) { 
                    return callback(err);
                }
                callback(null, result.transaction.command[0].add_block.block_name);
            });
        },
        /**
         * Params:
         * - logic: logic possibly containing placeholder ?'s
         * - args: logic arguments
         * - name: block to execute
         * - returnLocal: local predicates to return
         */
        exec: function(workspace, params, callback) {
            defaultApi.request(transaction(workspace, false, [{
                exec: {
                    block_name: params.name || null,
                    logic: fillPlaceHolders(params.logic, params.args || []),
                    return_local: params.returnLocal || []
                }
            }]), function(err, result) {
                if(err) {
                    return callback(err);
                }
                //var slimResult = result.transaction.command[0].exec;
                callback(null, result);
            });
        },
        /**
         * Convenience wrapper around exec
         */
        query: function(workspace, params, callback) {
            defaultApi.request(transaction(workspace, true, [{
                exec: {
                    logic: fillPlaceHolders(params.logic, params.args || []),
                    return_local: ["_"]
                }
            }]), function(err, result) {
                if (err) {
                    return callback(err);
                }
                callback(null, flipColumns(result.transaction.command[0].exec.return_local[0].column));
            });
        },
        queryPredicate: function(workspace, name, callback) {
            defaultApi.request(transaction(workspace, true, [{
                query_predicate: {
                    predicate: {
                        global_name: {
                            qualified_name: name
                        }
                    }
                }
            }]), function(err, result) {
                if (err) {
                    return callback(err);
                }
                callback(null, flipColumns(result.transaction.command[0].query_predicate.relation.column));
            });
        },
        // Meta
        listPredicates: function(workspace, callback) {
            defaultApi.request(transaction(workspace, true, [{
                pred_info_bulk: {}
            }]), function(err, result) {
                if (err) {
                    return callback(err);
                }
                callback(null, result.transaction.command[0].pred_info_bulk.info);
            });
        },
        predInfo: function(workspace, name, callback) {
            defaultApi.request(transaction(workspace, true, [{
                pred_info: {
                    predicate: {
                        global_name: {
                            qualified_name: name
                        }
                    }
                }
            }]), function(err, result) {
                if (err) {
                    return callback(err);
                }
                callback(null, result.transaction.command[0].pred_info.info);
            });
        },
        close: function(callback) {
            client.end();
            callback();
        }
    };

    var adminApi = {
        request: function(json, callback) {
            var r = new AdminRequest(json);
            var buffer = r.encode().toBuffer();
            reqId++;
            outstandingRequests[reqId] = callback;
            client.write(intToBuf(reqId));
            client.write(intToBuf(buffer.length));
            client.write(buffer);
        },
        listWorkspaces: function(callback) {
            adminApi.request({
                list_workspaces: true
            }, function(err, result) {
                if (err) {
                    return callback(err);
                }
                callback(null, result.list_workspaces.name);
            });
        },
        getStatus: function(workspaces, callback) {
            adminApi.request({
                status: {
                    workspaces: workspaces,
                    show_active_requests: true,
                    show_queued_requests: true,
                }
            }, function(err, result) {
                if (err) {
                    return callback(err);
                }
                callback(null, result.status.workspaces);
            });
        },
        close: function(callback) {
            client.end();
            callback();
        }
    };

    api = isAdmin ? adminApi : defaultApi;

    // Make all async methods thunk-capable so that we can do cool yield-generator stuff
    _.each(api, function(fn, name) {
        api[name] = function() {
            var lastArg = arguments[arguments.length - 1];
            if (!_.isFunction(lastArg)) {
                return _.partial.apply(_, [fn].concat(_.toArray(arguments)));
            } else {
                return fn.apply(api, arguments);
            }
        };
    });

    var waitingForBytes = 0;
    var receivingReqId;
    var intermediateBuffer;

    function parseAndHandle(reqId, buffer) {
        var callback = outstandingRequests[reqId];
        delete outstandingRequests[reqId];
        var resp = (isAdmin ? AdminResponse : RegularResponse).decode(buffer);
        if (resp.exception) {
            callback(resp.exception);
        } else {
            callback(null, resp);
        }
    }

    client.on('data', function(data) {
        // console.log("Got buffer.");
        if (waitingForBytes > 0) {
            // console.log("Appending it to ongoing request");
            if (data.length === waitingForBytes) {
                intermediateBuffer = Buffer.concat([intermediateBuffer, data]);
                parseAndHandle(receivingReqId, intermediateBuffer);
                intermediateBuffer = null;
                waitingForBytes = 0;
            } else if (data.length < waitingForBytes) {
                intermediateBuffer = Buffer.concat([intermediateBuffer, data]);
                waitingForBytes -= data.length;
            } else { // >
                intermediateBuffer = Buffer.concat([intermediateBuffer, data.slice(0, waitingForBytes)]);
                parseAndHandle(receivingReqId, intermediateBuffer);
                // Push data back for later processing
                client.unshift(data.slice(waitingForBytes));
                intermediateBuffer = null;
                waitingForBytes = 0;
            }
        } else {
            var requestId = bufToInt(data.slice(0, 4));
            var len = bufToInt(data.slice(4, 8));
            // console.log("RequestID", requestId, "len", len);
            var buffer = data.slice(8);
            if (buffer.length < len) {
                intermediateBuffer = buffer;
                waitingForBytes = len - buffer.length;
                receivingReqId = requestId;
                console.log("Waiting for more");
            } else {
                parseAndHandle(requestId, buffer);
            }
        }
    });

    client.on('error', function(e) {
        console.error("Error", e);
    });
};