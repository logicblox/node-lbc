/**
 * Safer REPL server, limits executable commands and
 * chooses random workspace name
 * 
 * Usage:
 * 
 * Start a new session:
 * 
 *   curl -X POST http://localhost:8086/repl
 * 
 * returns an 'id' to use in future URLs
 * 
 * Add a block (replace <id> with the id returned from creating session):
 * 
 * curl -X POST -H Content-Type=application/json \
 *      -d '{"logic": "person(x) -> string(x)."}' \
 *      http://localhost:8086/repl/<id>/addblock
 * 
 * Exec a block:
 * 
 * curl -X POST -H Content-Type=application/json \
 *      -d '{"logic": "+person(\"Zef\")."}' \
 *      http://localhost:8086/repl/<id>/exec
 *
 * Query:
 * 
 * curl -X POST -H Content-Type=application/json \
 *      -d '{"logic": "_(p) <- person(p)."}' \
 *      http://localhost:8086/repl/<id>/query
 *
 * List predicates:
 * 
 * curl http://localhost:8086/repl/<id>/predicates
 */

var lbConnect = require("../lbc");
var express = require("express");

var app = express();

app.use(express.logger());
app.use(express.json());

lbConnect(function(err, conn) {

    app.post("/repl", function(req, res) {
        var uuid = require("node-uuid").v4();
        conn.createWorkspace(uuid, function(err) {
            if (err) {
                res.status(500);
                return res.send({
                    status: "error",
                    message: "Could not create new workspace, try again later."
                });
            }
            res.status(500);
            res.send({
                status: 'ok',
                id: uuid
            });
        });
    });

    app.post("/repl/:ws/addblock", function(req, res) {
        var ws = req.params.ws;
        var logic = req.body.logic;
        if (!logic) {
            res.status(500);
            return res.send({
                status: "error",
                message: "No logic supplied"
            });
        }
        
        conn.addBlock(ws, logic, function(err) {
            if(err) {
                res.status(500);
                return res.send({
                    status: "error",
                    message: "Could not addblock:" + err.message
                });
            } else {
                res.status(200);
                res.send({status: 'ok'});
            }
        });
    });
    
    app.post("/repl/:ws/exec", function(req, res) {
        var ws = req.params.ws;
        var logic = req.body.logic;
        if (!logic) {
            res.status(500);
            return res.send({
                status: "error",
                message: "No logic supplied"
            });
        }
        
        conn.exec(ws, logic, function(err) {
            if(err) {
                res.status(500);
                return res.send({
                    status: "error",
                    message: "Could not exec: " + err.message
                });
            } else {
                res.status(200);
                res.send({status: 'ok'});
            }
        });
    });
    
    app.post("/repl/:ws/query", function(req, res) {
        var ws = req.params.ws;
        var logic = req.body.logic;
        if (!logic) {
            res.status(500);
            return res.send({
                status: "error",
                message: "No logic supplied"
            });
        }
        
        conn.query(ws, logic, function(err, result) {
            if(err) {
                res.status(500);
                return res.send({
                    status: "error",
                    message: "Could not query: " + err.message
                });
            } else {
                res.status(200);
                res.send({status: 'ok', result: result});
            }
        });
    });
    
    app.get("/repl/:ws/list", function(req, res) {
        var ws = req.params.ws;
        
        conn.listPredicates(ws, function(err, result) {
            if(err) {
                res.status(500);
                return res.send({
                    status: "error",
                    message: "Could not list predicates: " + err.message
                });
            } else {
                res.status(200);
                res.send({status: 'ok', predicates: result});
            }
        });
    });
    
    console.log("Listening on port 8086...");
    app.listen(8086);
});