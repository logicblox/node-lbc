/*

 Run this server with:

 $ node --harmony lb_api_server.js

 Curl API:

 $ curl --user admin:secure http://workspaces.logicblox.com:8086/users
 $ curl --user admin:secure http://workspaces.logicblox.com:8086/users -X POST -d '{"user": "zef", "key": "1234"}' -H 'Content-type: application/json'
 $ curl --user admin:secure http://workspaces.logicblox.com:8086/workspaces -X POST -d '{"name": "zefspace"}' -H 'Content-type: application/json'
 $ curl --user admin:secure http://workspaces.logicblox.com:8086/workspaces/zefspace/perms
 $ curl --user admin:secure http://workspaces.logicblox.com:8086/workspaces/zefspace/perms -X PUT -d '{"zef": ["addblock", "exec", "query", "list"]}' -H 'Content-type: application/json'
 $ curl --user zef:1234     http://workspaces.logicblox.com:8086/workspaces/zefspace/predicates
 $ curl --user zef:1234     http://workspaces.logicblox.com:8086/workspaces/zefspace/addblock -X POST -d '{"logic": "person(x) -> string(x)."}'  -H 'Content-type: application/json'
 $ curl --user zef:1234     http://workspaces.logicblox.com:8086/workspaces/zefspace/predicates/person
 $ curl --user zef:1234     http://workspaces.logicblox.com:8086/workspaces/zefspace/exec -X POST -d '{"logic": "+person(\"Zef\")."}'  -H 'Content-type: application/json'
 $ curl --user zef:1234     http://workspaces.logicblox.com:8086/workspaces/zefspace/query -X POST -d '{"logic": "_(p) <- person(p)."}' -H 'Content-type: application/json'

*/

var lbConnect = require('..');
var co = require('co');
var fs = require('co-fs');
var express = require('express');
var _ = require("lodash");

var USER_WORSPACE = "users";
var ALL_PERMS = ["addblock", "exec", "query", "list"];


var app = require("co-express")(express());
app.use(express.logger());
app.use(express.json());

function safeString(s) {
    return !!/^[\w\-]+$/.exec(s);
}

function cleanResults(results) {
    return _.map(results, function(result) {
        return _.pluck(result, 'jsValue');
    });
}

// co is a nifty library that allows you to write async code in a synchronous-looking style
// using ES6 generators (which is why it only works with node 0.11+ with the --harmony flag)
co(function*() {
    console.log("Connecting to LB");
    var conn = yield lbConnect();

    // Ugly way to check if workspace exists
    try {
        yield conn.listPredicates(USER_WORSPACE);
    } catch (e) {
        // If not, initialize!
        yield initDatabase();
    }

    // Authenticate all the calls!
    app.use(express.basicAuth(function(user, key, callback) {
        co(function*() {
            if (!safeString(user) || !safeString(key)) {
                throw Error("Invalid character for auth");
            }
            var query = '_(user, key, isadmin) <- user = "' + user + '", key = "' + key + '",  user_name[u] = user, user_key[u] = key, (user_is_admin(u), isadmin = true; !user_is_admin(u), isadmin = false).';
            var results = yield conn.query(USER_WORSPACE, query);
            if (results.length === 0) {
                throw Error("Invalid username or key");
            }
            return {
                user: user,
                isAdmin: results[0][2].jsValue
            };
        })(callback);
    }));

    // Root URL just gives you back user info
    app.get("/", function(req, res) {
        res.send(req.user);
    });

    app.post("/users", function * (req, res) {
        if (!req.user.isAdmin) {
            throw Error("Only admins can create users");
        }
        var user = req.body.user;
        var key = req.body.key;
        if(!user) throw Error("No user supplied.");
        if(!key) throw Error("No key supplied.");
        if(!safeString(user)) throw Error("User contains invalid characters");
        if(!safeString(key)) throw Error("Key contains invalid characters");
        yield conn.exec(USER_WORSPACE, '+user_key["' + user + '"] = "' + key + '".');
        res.send({status: "ok"});
    });

    app.get("/users", function * (req, res) {
        if (!req.user.isAdmin) {
            throw Error("Only admins can list users");
        }
        var results = yield conn.queryPredicate(USER_WORSPACE, "user_name");
        var users = _.map(results, function(user) { return user[1].jsValue; });
        res.send({status: "ok", users: users});
    });

    app.post("/workspaces", function * (req, res) {
        if (!req.user.isAdmin) {
            throw Error("Only admins can create workspaces");
        }
        var name = req.body.name;
        if(!name) throw Error("No workspace name supplied.");
        if(!safeString(name)) throw Error("Name contains invalid characters");
        yield conn.createWorkspace(name);
        res.send({status: "ok"});
    });

    // { username: ["exec", "addblock", "query"], ... }
    app.put("/workspaces/:ws/perms", function*(req, res) {
        if (!req.user.isAdmin) {
            throw Error("Only admins can set workspace permissions");
        }
        var ws = req.params.ws;
        var users = req.body;
        var parts = [];
        _.each(ALL_PERMS, function(perm) {
            parts.push('-workspace_permission_' + perm + '(ws, user) <- ws = "' + ws + '", workspace_permission_' + perm + '@prev(ws, user).\n');
        });
        var wipeQuery = parts.join("");
        yield conn.exec(USER_WORSPACE, wipeQuery);
        parts = [];
        _.each(users, function(perms, user) {
            _.each(perms, function(perm) {
                parts.push('+workspace_permission_' + perm + '("' + ws + '", "' + user + '")');
            });
        });
        var query = parts.join(", ") + ".";
        if(query !== ".") {
            yield conn.exec(USER_WORSPACE, query);
        }
        res.send({status: "ok"});
    });

    app.get("/workspaces/:ws/perms", function*(req, res) {
        if (!req.user.isAdmin) {
            throw Error("Only admins can set workspace permissions");
        }
        var ws = req.params.ws;
        var parts = [];
        _.each(ALL_PERMS, function(perm) {
            parts.push('(type = "' + perm + '", workspace_permission_' + perm + '("' + ws + '", user))');
        });
        var query = '_(type, user) <- ' + parts.join(";") + '.';
        var results = yield conn.query(USER_WORSPACE, query);
        var users = {};
        _.each(results, function(result) {
            var user = result[1].jsValue;
            if(!users[user]) {
                users[user] = [];
            }
            users[user].push(result[0].jsValue);
        });
        res.send({status: 'ok', perms: users});
    });

    // Workspace operations
    app.get("/workspaces/:ws/predicates", function*(req, res) {
        var ws = req.params.ws;
        if(!(yield hasPermission(req.user.user, ws, "list"))) throw Error("Not allowed");

        res.send({status: "ok", predicates: yield conn.listPredicates(ws)});
    });

    app.get("/workspaces/:ws/predicates/:pred", function*(req, res) {
        var ws = req.params.ws;
        var pred = req.params.pred;
        if(!(yield hasPermission(req.user.user, ws, "query"))) throw Error("Not allowed");

        res.send({status: "ok", results: cleanResults(yield conn.queryPredicate(ws, pred))});
    });

    app.post("/workspaces/:ws/addblock", function*(req, res) {
        var ws = req.params.ws;
        if(!(yield hasPermission(req.user.user, ws, "addblock"))) throw Error("Not allowed");

        var logic = req.body.logic;
        if(!logic) throw Error("No logic supplied.");

        yield conn.addBlock(ws, logic);

        res.send({status: "ok"});
    });

    app.post("/workspaces/:ws/exec", function*(req, res) {
        var ws = req.params.ws;
        if(!(yield hasPermission(req.user.user, ws, "exec"))) throw Error("Not allowed");

        var logic = req.body.logic;
        if(!logic) throw Error("No logic supplied.");

        yield conn.exec(ws, logic);

        res.send({status: "ok"});
    });

    app.post("/workspaces/:ws/query", function*(req, res) {
        var ws = req.params.ws;
        if(!(yield hasPermission(req.user.user, ws, "query"))) throw Error("Not allowed");

        var logic = req.body.logic;
        if(!logic) throw Error("No logic supplied.");

        res.send({status: "ok", results: cleanResults(yield conn.query(ws, logic))});
    });

    // If any of the above raised an exception, present it quasi-nicely here
    app.use(function(err, req, res, next) {
        if (err) {
            console.error("Error handling request: ", err);
            res.status(500);
            res.send({
                status: "error",
                message: err.message
            });
        } else {
            next();
        }
    });

    console.log("Listening on port 8086");
    app.listen(8086);

    function* hasPermission(user, ws, perm) {
        var query = '_(1) <- user_can_' + perm + '("' + user + '", "' + ws + '").';
        var results = yield conn.query(USER_WORSPACE, query);
        return results.length > 0;
    }

    function* initDatabase() {
        var schemaLogic = yield fs.readFile("api_server_schema.logic", "utf8");
        var initLogic = yield fs.readFile("api_server_init.logic", "utf8");
        yield conn.createWorkspace(USER_WORSPACE);
        yield conn.addBlock(USER_WORSPACE, schemaLogic);
        yield conn.exec(USER_WORSPACE, initLogic);
    }
})(function(err) {
    if (err) {
        console.error(err.message);
        throw err;
    }
});