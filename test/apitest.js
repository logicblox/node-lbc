var lbConnect = require('..');
var co = require('co');
var assert = require("assert");
var exec = require('co-exec');

var wsName = "lbtest";

co(function*() {
    console.log("Connecting to LB");
    var conn = yield lbConnect();
    var adminConn = yield lbConnect({admin: true});

    console.log("Cleaning up");
    var workspaces = yield adminConn.listWorkspaces();
    for(var i = 0; i < workspaces.length; i++) {
        var ws = workspaces[i];
        yield conn.removeWorkspace(ws);
    }
    
    console.log("Creating workspace");
    yield conn.createWorkspace(wsName);
    
    console.log("Creating anonymous workspace");
    var result = yield conn.createWorkspace(null);
    assert.equal(typeof result, "string", "Workspace creation returned a workspace name");

    console.log("Adding block");
    var result = yield conn.addBlock(wsName, {
        name: "myblock",
        logic: "p(a, b, c, d, e, f) -> int(a), decimal(b), float(c), string(d), boolean(e), datetime(f)."
    });
    assert.equal(result, "myblock");

    console.log("Inspecting resulting predicate");
    var predicates = yield conn.listPredicates(wsName);
    assert.equal(predicates.length, 1, "number of defined predicates");
    var predInfo = yield conn.predInfo(wsName, "p");
    assert.equal(predInfo.name, "p");

    console.log("Inserting some data and verifying it");
    yield conn.exec(wsName, {
        logic: '+p(1, 1.5d, 1.5f, "Zef", true, datetime:now[]).'
    });
    yield conn.exec(wsName, {
        logic: '+p(?, ?d, ?f, ?, ?, datetime:now[]).',
        args: [2, 2.5, 2.5, "Zef2", true]
    });
    yield conn.exec(wsName, {
        logic: '+p(2, 2.5d, 2.5f, "Zef2", false, datetime:now[]).'
    });

    function checkResults(results) {
        assert.equal(results[0][0].value, '1');
        assert.equal(results[0][1].value, '150000');
        assert.equal(results[0][2].value, 1.5);
        assert.equal(results[0][3].value, "Zef");
        assert.equal(results[0][4].value, true);
        assert.equal(results[1][0].value, '2');
        assert.equal(results[1][1].value, '250000');
        assert.equal(results[1][2].value, 2.5);
        assert.equal(results[1][3].value, "Zef2");
        assert.equal(results[1][4].value, false);
    }

    var results = yield conn.queryPredicate(wsName, "p");
    checkResults(results);
    results = yield conn.query(wsName, {
        logic: '_(a, b, c, d, e, f) <- p(a, b, c, d, e, f).'
    });
    checkResults(results);

    console.log("Exporting and importing workspace");
    yield exec('rm -rf /tmp/lbdb');
    yield conn.exportWorkspace(wsName, "/tmp/lbdb");
    yield conn.importWorkspace("lbtest2", "/tmp/lbdb");
    predicates = yield conn.listPredicates("lbtest2");
    assert.equal(predicates.length, 1);
    assert.equal(predicates[0].name, "p");
    yield conn.removeWorkspace("lbtest2");
    yield conn.close();
    yield adminConn.close();
})(function(err) {
    if (err) {
        console.error(err.message);
        throw err;
    }
});