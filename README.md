node-lbc
========

A node.js client library for LogicBlox. Allows you to connect to LogicBlox using pure JavaScript running in node.js.

It is the product of the LogicBlox Hackathon '13 and written by [Zef Hemel](mailto:zef.hemel@logicblox.com). A 5 minute screencast explaining it
[can be seen here](http://screencast.com/t/zZ7uBddiRQg).

Why a node.js connector?
------------------------

We now position LogicBlox as a database. Typically, a database is not enough to build complete applications. You typically have an application server of some kind that talks to the database and executes queries. On the Java platform you have JDBC for SQL databases. I already built a Clojure connector for LogicBlox before, so this time I thought I'd try building one for [node.js](http://nodejs.org).

The cool thing about node.js is

1. It's JavaScript -- awesome
2. It's asynchronous
3. It's a great platform for building efficient servers that primarily glue systems together

How to use it
=============

The project contains a Vagrantfile:

    $ vagrant up

Once done (this will take a while), login:

    $ vagrant ssh

and then run `npm install` to intall dependencies:

    $ npm install


To run the tests (in `tests`):

    $ npm test

Connect
-------

Import the module:

    var lbConnect = require("lbc");

Connect to LB:

    lbConnect(function(err, conn) {
        // Your code here
        // Close connection
        conn.close();
    });

Disconnect:

    conn.close(function(err) {
       ...
    });

Schema management
-----------------

Add a block:

    conn.addBlock("myworkspace", "person(x) -> string(x).", function(err) {
       ...
    });

Queries
-------

Execute a block:

    conn.exec("myworkspace", "+person(\"Zef\").", function(err) {
       ...
    });

Query a predicate:

    conn.query("myworkspace", "person", function(err, results) {
       console.log(results);
    });

Execute a query:

    conn.query("myworkspace", "_(p) <- person(p).", function(err, results) {
       console.log(results);
    });

Workspace management
--------------------

Create a workspace:

    conn.createWorkspace("myworkspace", function(err) {
       ...
    });

Reset a workspace (create with overwrite):

    conn.resetWorkspace("myworkspace", function(err) {
       ...
    });

Export a workspace:

    conn.exportWorkspace("myworkspace", "/tmp/myws", function(err) {
       ...
    });

Import a workspace:

    conn.importWorkspace("myworkspace2", "/tmp/myws", function(err) {
       ...
    });

Remove a workspace:

    conn.removeWorkspace("myworkspace2", function(err) {
       ...
    });

Sample applications
===================

The `sample/` directory contains two samples:

* `replserver.js`: a server that could be used as a more secure connectblox implementation (used now at https://repl.logicblox.com).
* `lb_api_server.js` which is described in the next section.

LB API Server
-------------

Most, if not practically all databases that I know of have some sort of authentication and authorization system. LogicBlox does not. In fact, support
for connecting to a LB database from another system is very limited.

The LB API server (`samples/lb_api_server.js`) exposes a LogicBlox server via a HTTP RESTful API and adds support for multiple users and authorization of users for specific workspaces, so you can create a user (as an admin) that can query a workspace, but not run 'exec' queries, or have access to other workspaces on the same server.

Documentation is limited due to time constraints, but the curl commands at the
top of `lb_api_server.js` should help understanding the idea.

The users and their permissions are stored in a `users` workspace which is automatically created when the server is
first started (and it does not already exist). The schema
for this can be found under `sample/api_server_schema.logic`, as well as the initial data (for the `admin` user) in `sample/api_server_init.logic`.